# Flaskでの画像分類アプリ

1. https://github.com/ericsun99/MobileNet-V2-Pytorch からPretainedModel（mobilenetv2_Top1_71.806_Top2_90.410.pth.tar）を取得
2. modelディレクトリ下に上モデルを置く
3. ```python app.py```でFlaskを稼働
4. http://address:8080にブラウザでアクセス。

※全ラベルはhttp://address:8080/labelsにアクセスすれば確認できます。