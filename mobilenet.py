from collections import OrderedDict

import numpy as np
from PIL import Image

import torch
import torch.backends.cudnn as cudnn
import torchvision.transforms as transforms
from models.MobileNetV2 import MobileNetV2

from index2name import index2name


def inference(image):
    model = MobileNetV2()

    filename = "mobilenetv2_Top1_71.806_Top2_90.410.pth.tar"

    print("=> loading model '{}'".format(filename))
    data = torch.load("models/"+filename, map_location="cpu")
    data2 = OrderedDict([(k.replace("module.", ""),v) for k,v in data.items()])
    model.load_state_dict(data2)
    model.eval()
    cudnn.benchmark = True

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    transform = transforms.Compose([
        transforms.ToTensor(),
        normalize,
    ])

    #image = Image.open("test.png")
    #image = image.resize([224,224]).convert("RGB")
    xdata = transform(image)
    ydata = model(xdata.unsqueeze(0))
    proba = ydata.data.numpy()[0]
    rank  = np.argsort(proba)[-1:-1-5:-1]
    print(rank.shape)
    top5 = []
    for i in range(5):
        index = rank[i]
        s_proba = f"{proba[index]:.2f}%"
        s_index = f"{index}"
        s_name  = f"{index2name[index]}"
        top5.append([s_index, s_proba, s_name])

    return top5


if __name__ == '__main__':
    main()

