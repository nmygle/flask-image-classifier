import logging
import os,re
import datetime

from PIL import Image
from flask import Flask, render_template, request, redirect, url_for
from werkzeug import secure_filename

from mobilenet import inference
from index2name import index2name

app = Flask(__name__)

UPLOAD_FOLDER = "./static/uploads"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.',1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods=["POST"])
def do_upload():
    if request.method == "POST":
        img_file = request.files['img_file']
        if img_file and allowed_file(img_file.filename):
            filename = datetime.datetime.now().strftime('%Y%m%d%H%M%S') + '.png'
            img_file.save(os.path.join(app.config["UPLOAD_FOLDER"], filename))

            img = Image.open(img_file).convert('RGB').resize([224,224])

            top5 = inference(img)
            print(top5)
            return render_template('result.html', filename=filename, top=top5)
        else:
            return 'No file uploaded.', 400

@app.route('/labels')
def show_label():
    labels = [[k,v] for k,v in index2name.items()]
    return render_template('labels.html', labels=labels)

@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(e), 500


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=False)

